from pymongo import MongoClient
from datetime import timedelta
from bson import ObjectId

cli = MongoClient()

cursor = cli.trackerdb.trips.find({"schoolId" : ObjectId("565e7918ac628c049df68074")})
for item in cursor:
    print(item['imei'], item['startTime'])
    cli.trackerdb.trips.update({'_id': item['_id']}, {'$set': {'startTime': item['startTime'] - timedelta(0, 19800), 'endTime': item['endTime'] - timedelta(0, 19800)}})
cursor.close()

cli.close()

