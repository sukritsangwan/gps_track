from datetime import datetime

from pymongo import MongoClient, errors

cli = MongoClient()
# cli.the_database.authenticate('localUser', 'protip007', source='admin')

devices = cli.trackingdb.collection_names()

device = "356307045161881"
print(device)
# cli.trackingdb[device].update({}, {'$rename': {'io_data': 'status'}}, False, True)
# cli.trackingdb[device].update({}, {'$rename': {'status.acc': 'status.ac'}}, False, True)
# cur = cli.trackingdb[device].find()
# for record in cur:
#     if 'protocol_extra' in record:
#         continue
#     proto_extra = {'no_of_sat': record['no_of_sat'], 'io_event': record['io_event'], 'priority': record['priority']}
#     cli.trackingdb[device].update({'_id': record['_id']},
#                                   {'$set': {'protocol_extra': proto_extra},
#                                    '$unset': {'no_of_sat': False, 'io_event': False, 'priority': False}})
print('.....')

tk06a_devices = [v for v in devices if v.startswith('3554') and len(v) == 15]

root_fields = {'time': True, 'lat': True, 'lng': True, 'alt': True, 'speed': True, 'direction': True}
gsm_fields = {'mcc': True, 'mnc': True, 'lac': True, 'cellid': True}
protocol_fields = {'gps_type': True, 'is_gps_located': True, 'satellites': True, 'protocol_no': True}

for device in tk06a_devices:
    start_time = datetime.now()
    print(device)
    cur = cli.trackingdb[device].find()
    status = None
    i = 0
    tt = datetime.now()
    for record in cur:
        i += 1
        if i % 25000 == 0:
            print(i, datetime.now() - tt)
            tt = datetime.now()
        if 'protocol_extra' in record:
            continue
        if record['protocol_no'] == 19:
            status = record
            try:
                cli.trackingdb[device + "_others"].insert_one(record)
            except errors.DuplicateKeyError:
                pass
            continue
        if 'insertedAt' not in record:
            if 'time' in record:
                record['insertedAt'] = record['time']
            else:
                continue
        new_record = {'insertedAt': record['insertedAt'], 'gsm_data': {}, 'status': status, 'protocol_extra': {}}
        for f in record:
            if f in root_fields:
                new_record[f] = record[f]
            elif f in gsm_fields:
                new_record['gsm_data'][f] = record[f]
            elif f in protocol_fields:
                new_record['protocol_extra'][f] = record[f]
        # for f in root_fields:
        #     if f in record:
        #         new_record[f] = record[f]
        # for f in gsm_fields:
        #     if f in record:
        #         new_record['gsm_data'][f] = record[f]
        # for f in protocol_fields:
        #     if f in record:
        #         new_record['protocol_extra'][f] = record[f]
        cli.trackingdb[device].update({'_id': record['_id']}, new_record)
    cli.trackingdb[device].remove({'protocol_no': 19})
    print(device, datetime.now() - start_time)
    print('.....')
