// Date: 20 April 2016

tdb = db.getSisterDB('trackingdb');

print('removing records that have invalid lat lng from trackingdb (all collections)');

var colls = tdb.getCollectionNames();

colls.forEach(function(coll){
    if(coll != "system.indexes"){
        print("collection: " + coll);
        print("number of total records: " + tdb[coll].count().toString());
        print("number of invalid records: " + tdb[coll].count({lat: 0, lng: 0}).toString());
        tdb[coll].remove({lat: 0, lng: 0});
        print("removed invalid records from " + coll + "\n")
    }
});
