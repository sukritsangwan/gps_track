from pymongo import MongoClient
from datetime import timedelta

cli = MongoClient()

colls = [
	"355488020136597",
	"355488020136785",
	"355488020136826",
	"355488020136829",
	"355488020137011",
	"355488020137906",
	"355488020137942",
	"355488020138421",
	"355488020138435",
	"355488020138446",
	"355488020138454",
	"355488020138473",
	"355488020138475",
	"355488020138478",
	"355488020138490",
	"355488020138551",
	"355488020138583",
	"355488020145532"
]

for coll in colls:
    cursor = cli.trackingdb[coll].find({'time': {'$exists': True}})
    for item in cursor:
        cli.trackingdb[coll].update({'_id': item['_id']}, {'$set': {'time': item['time'] - timedelta(0, 19800) }})
    cursor.close()

cli.close()

