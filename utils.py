from datetime import datetime
from os import listdir
from os.path import isdir, join

mongodb = None


def dump_data(imei, data_dict):
    data_dict['insertedAt'] = datetime.now()
    coll_name = imei + "_others"
    if has_valid_coordinates(data_dict):
        data_dict['loc'] = {'type': "Point", 'coordinates': [data_dict['lng'], data_dict['lat']]}
        coll_name = imei

    result = mongodb[coll_name].insert_one(data_dict)
    return {'_id': result.inserted_id}


def has_valid_coordinates(data):
    if 'lat' not in data or 'lng' not in data:
        return False
    # what if lat and lng are perfectly (0, 0); Highly improbable and not so critical
    if data['lat'] == 0 or data['lng'] == 0:
        return False
    return True


def get_commands(imei):
    cur = mongodb['commands'].find({'imei': imei, 'to_send': True})
    commands = [x for x in cur]
    cur.close()
    return commands


def command_mark_sent(_id):
    return mongodb['commands'].update({'_id': _id}, {'$set': {'to_send': False}})


def get_int(data_bytes):
    if isinstance(data_bytes, int):
        return data_bytes
    return int.from_bytes(data_bytes, 'big')


def get_bool(bin_char):
    if bin_char == '0':
        return False
    elif bin_char == '1':
        return True


class DisconnectError(Exception):
    """Raised to signal disconnect from client"""
    def __init__(self, msg):
        self._msg = msg

    def __str__(self):
        return self._msg


def get_open_fds():
    proc_dict = {}
    proc_path = '/proc'
    procs_list = listdir(proc_path)
    for proc_id in procs_list:
        n_path = join(proc_path, proc_id)
        if isdir(n_path):
            if 'fd' in listdir(n_path):
                open_fd_count = len(listdir(join(n_path, 'fd')))
                with open(join(n_path, 'cmdline')) as f:
                    proc_command = f.read()
                proc_dict[proc_id] = {'count': open_fd_count, 'command': proc_command}
    return proc_dict
