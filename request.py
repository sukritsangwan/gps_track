import crc16

from http import server


class Request:
    def __init__(self, raw_data):
        self.raw_data = raw_data

        self.header = raw_data[0:2]
        # validate header
        if self.header != 'xx':
            raise Exception("invalid request. first 2 bytes should be xx. found " + self.header)

        self.length = int(raw_data[2:3])
        self.proto_no = raw_data[3:4]
        self.device_id = str(raw_data[4:11])
        self.command = raw_data[11:13]
        self.data = raw_data[14:-4]
        self.checksum = raw_data[-4:-2]


        # if crc16.crc16xmodem(raw_data[:-2]) != self.checksum:
        #     raise Exception("CRC failed")


def make_response(data, request):
    response = bytes()

    header = (0x7878).to_bytes(2, 'big')
    response = response.__add__(header)

    length = (2 + 2 + 7 + 2 + len(data) + 2 + 2). to_bytes(2, byteorder='big')
    response = response.__add__(length)

    device_id = bytes(request.device_id, 'ascii')
    response = response.__add__(device_id)

    command = request.command
    response = response.__add__(command)

    if not data:
        data = bytes()
    response = response.__add__(data)


    checksum = crc16.crc16xmodem(response)
    checksum = checksum.to_bytes(2, 'big')
    response = response.__add__(checksum)

    response = response.__add__(bytes('\r\n', 'ascii'))

    # @@<L><ID><0x9956 ><Flag><checksum> \r\n
    return response
