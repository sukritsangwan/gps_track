from ast import literal_eval
import logging
from datetime import datetime

from utils import dump_data, DisconnectError

logger = logging.getLogger(__name__)

HEADER = (0x8989).to_bytes(2, 'big')
h_b = 0x89.to_bytes(1, 'big')


def read_header(client, remote_addr):
    while True:
        c = client.recv(1)
        if not c:
            raise DisconnectError("")
        if c == h_b:
            c = client.recv(1)
            if not c:
                raise DisconnectError("")
            if c == h_b:
                return


def handle(client, remote_addr):
    # read length
    len_byte = client.recv(2)
    length = int.from_bytes(len_byte, 'big')

    info_content = client.recv(length)

    data_dict = literal_eval(info_content.decode('ascii'))
    client.imei = data_dict['imei']
    del data_dict['imei']
    if '_id' in data_dict:
        del data_dict['_id']

    data_dict['time'] = datetime.fromtimestamp(data_dict['time'])

    logger.debug(data_dict)

    dump_data(client.imei, data_dict)

    return
