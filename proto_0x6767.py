from datetime import datetime
import logging
import binascii
from utils import DisconnectError, get_int, dump_data, get_bool

logger = logging.getLogger(__name__)

HEADER = (0x6767).to_bytes(2, 'big')
h_b = 0x67.to_bytes(1, 'big')


def read_header(client, remote_addr):
    while True:
        c = client.recv(1)
        if not c:
            raise DisconnectError("")
        if c == h_b:
            c = client.recv(1)
            if not c:
                raise DisconnectError("")
            if c == h_b:
                return


def handle(client, remote_addr):
    proto_no = client.recv(1)

    # read length
    len_byte = client.recv(2)
    length = int.from_bytes(len_byte, 'big')

    info_serial_no = client.recv(2)
    client.info_serial_no = info_serial_no

    data_length = length - 2

    info_content = client.recv(data_length)

    logger.debug({'header': HEADER, 'length': length, 'proto_no': proto_no, 'info_content': info_content,
                  'info_serial_no': info_serial_no})

    # process msg
    response_content = process_request(proto_no, info_content, client)

    if response_content is None:
        # no need to send response
        return

    # calculate response length
    resp_length = len(response_content) + 2
    len_byte = resp_length.to_bytes(2, 'big')

    # return response
    response = HEADER + proto_no + len_byte + info_serial_no + response_content

    return response


def construct_command(client, remote_addr, command_type, command_data):
    command = None
    if command_type == 0x00:
        command = command_data
    else:
        raise DisconnectError("command_type not supported")

    client.info_serial_no += 1
    cmd_length = 2 + 1 + 4 + len(command)
    len_byte = cmd_length.to_bytes(2, 'big')
    cmd_sign = 0x01.to_bytes(1, 'big')
    flag = 0x78563412.to_bytes(4, 'big')
    msg_data = HEADER + 0x80.to_bytes(1, 'big') + len_byte + client.info_serial_no + cmd_sign + flag + command

    if not hasattr(client, 'server_flags'):
        client.server_flags = {}
    client.server_flags[flag] = command

    logger.info({'imei': client.imei, 'flag': flag, 'command': command})

    return msg_data


def login(data, client):
    if not hasattr(client, 'imei') or client.imei == "unknown":
        imei = binascii.hexlify(data[:-2]).decode('ascii')[1:]
        client.imei = imei
    language = "Simplified Chinese"
    lang_byte = data[-2]
    if get_int(lang_byte) == 0x01:
        language = "English"
    tz_byte = data[-1]
    time_zone = get_int(tz_byte)
    logger.info({'imei': client.imei, 'protocol_no': '0x01', 'language': language, 'time_zone': time_zone})
    return b''


def gps_info(data, client):
    # first 6 bit is date time
    # next bit has length of gps data and no of satellite, e.g. 0x9C means length is 9 and no of sat is 12
    time = datetime.fromtimestamp(get_int(data[0:4]))

    lat = float(get_int(data[4:8])) / 1800000
    lng = float(get_int(data[8:12])) / 1800000

    speed = get_int(data[12])
    direction = get_int(data[13:15])

    # base_stn = data[15:24]
    mcc = get_int(data[15:17])
    mnc = get_int(data[17:19])
    lac = get_int(data[19:21])
    cellid = get_int(data[21:24])

    position_status_bit = bin(get_int(data[24]))[-1]
    position_status = "not positioned"
    if position_status_bit == "1":
        position_status = "positioned"

    gsm_data = {'mcc': mcc, 'mnc': mnc, 'lac': lac, 'cellid': cellid}
    protocol_extra = {'protocol_no': 0x02, 'position_status': position_status}
    data_dict = {'time': time, 'lat': lat, 'lng': lng, 'speed': speed, 'direction': direction,
                 'gsm_data': gsm_data, 'protocol_extra': protocol_extra, 'status': None}

    dump_data(client.imei, data_dict=data_dict)

    logger.debug({'imei': client.imei, 'data': data_dict})


def status_info(data, client):
    # first 6 bit is date time
    # next bit has length of gps data and no of satellite, e.g. 0x9C means length is 9 and no of sat is 12
    time = datetime.fromtimestamp(get_int(data[0:4]))

    lat = float(get_int(data[4:8])) / 1800000
    lng = float(get_int(data[8:12])) / 1800000

    speed = get_int(data[12])
    direction = get_int(data[13:15])

    # base_stn = data[15:24]
    mcc = get_int(data[15:17])
    mnc = get_int(data[17:19])
    lac = get_int(data[19:21])
    cellid = get_int(data[21:24])

    position_status_bit = bin(get_int(data[24]))[-1]
    position_status = "not positioned"
    if position_status_bit == "1":
        position_status = "positioned"

    status_type_byte = get_int(data[25])
    status_type_dict = {
        0x01: "acc ignition",
        0x02: "acc flameout",
        0x03: "digital input port status changed"
    }
    status_type = status_type_dict[status_type_byte]

    status_time = datetime.fromtimestamp(get_int(data[26:30]))
    device_status_bytes = data[30:32]
    device_status_dict = decode_heartbeat(device_status_bytes)

    gsm_data = {'mcc': mcc, 'mnc': mnc, 'lac': lac, 'cellid': cellid}
    protocol_extra = {'protocol_no': 0x05, 'position_status': position_status, 'status_time': status_time,
                      'status_type': status_type}

    data_dict = {'time': time, 'lat': lat, 'lng': lng, 'speed': speed, 'direction': direction,
                 'gsm_data': gsm_data, 'protocol_extra': protocol_extra, 'status': device_status_dict}

    dump_data(client.imei, data_dict=data_dict)

    logger.debug({'imei': client.imei, 'data': data_dict})
    return b''


def decode_heartbeat(data):
    device_info = get_int(data)
    bin_str = bin(device_info)[2:]
    while len(bin_str) < 16:
        bin_str = '0' + bin_str

    gps_positioned = get_bool(bin_str[-1])

    tmp_str = bin_str[-3:-1]
    if tmp_str == "00":
        acc = "none"
    elif tmp_str == "01":
        acc = "off"
    elif tmp_str == "11":
        acc = "on"

    tmp_str = bin_str[-5:-3]
    if tmp_str == "00":
        fortification = "none"
    elif tmp_str == "01":
        fortification = "disarming"
    elif tmp_str == "11":
        fortification = "fortification"

    tmp_str = bin_str[-7:-5]
    if tmp_str == "00":
        oil_power = "none"
    elif tmp_str == "01":
        oil_power = "cut"
    elif tmp_str == "11":
        oil_power = "restore"

    tmp_str = bin_str[-9:-7]
    if tmp_str == "00":
        charger = "none"
    elif tmp_str == "01":
        charger = "not_plugged"
    elif tmp_str == "11":
        charger = "plugged"

    data_dict = {'protocol_no': 0x13, 'gps_positioned': gps_positioned, 'acc': acc, 'fortification': fortification,
                 'oil_power': oil_power, 'charger': charger}

    return data_dict


def heartbeat(data, client):
    data_dict = decode_heartbeat(data)
    data_dict.update({'protocol_no': 0x03})

    dump_data(client.imei, data_dict=data_dict)

    logger.debug({'imei': client.imei, 'data': data_dict})
    return b''


def alarm(data, client):
    # first 6 bit is date time
    # next bit has length of gps data and no of satellite, e.g. 0x9C means length is 9 and no of sat is 12
    time = datetime.fromtimestamp(get_int(data[0:4]))

    lat = float(get_int(data[4:8])) / 1800000
    lng = float(get_int(data[8:12])) / 1800000

    speed = get_int(data[12])
    direction = get_int(data[13:15])

    # base_stn = data[15:24]
    mcc = get_int(data[15:17])
    mnc = get_int(data[17:19])
    lac = get_int(data[19:21])
    cellid = get_int(data[21:24])

    position_status_bit = bin(get_int(data[24]))[-1]
    position_status = "not positioned"
    if position_status_bit == "1":
        position_status = "positioned"

    alarm_byte = get_int(data[-1])
    alarm_dict = {
        0x01: "power off",
        0x02: "sos",
        0x03: "low battery",
        0x04: "vibration",
        0x05: "displacement",
        0x06: "enter blind area",
        0x07: "leave blind area",
        0x08: "gps open circuit",
        0x09: "gps short circuit",
        0x81: "low speed",
        0x82: "high speed",
        0x83: "in geofence",
        0x84: "out geofence",
        0x85: "collision",
        0x86: "drop"
    }
    alarm = alarm_dict[alarm_byte]

    if alarm_byte == 0x83 or alarm_byte == 0x84:
        # inform the application server
        pass

    gsm_data = {'mcc': mcc, 'mnc': mnc, 'lac': lac, 'cellid': cellid}
    protocol_extra = {'protocol_no': 0x04, 'position_status': position_status}

    data_dict = {'time': time, 'alarm': alarm, 'lat': lat, 'lng': lng, 'speed': speed, 'direction': direction,
                 'gsm_data': gsm_data, 'protocol_extra': protocol_extra, 'status': None}

    dump_data(client.imei, data_dict=data_dict)

    logger.debug({'imei': client.imei, 'data': data_dict})
    return b''


def sms_commands_up(data, client):
    return b''


def obd_data(data, client):
    return b''


def obd_fault_codes(data, client):
    return b''


def sms_commands_down(data, client):
    msg_sign = get_int(data[0])
    flag = get_int(data[1:5])
    content = data[5:]

    logger.info({'imei': client.imei, 'response': content, 'flag': flag, 'command': client.server_flags[flag]})

    del client.server_flags[flag]

    return


def ordinary_data(data, client):
    return b''


def photo_info(data, client):
    return b''


def photo_content(data, client):
    return b''


proto_dict = {
    0x01: login,
    0x02: gps_info,
    0x03: heartbeat,
    0x04: alarm,
    0x05: status_info,
    0x06: sms_commands_up,
    0x07: obd_data,
    0x09: obd_fault_codes,
    0x80: sms_commands_down,
    0x81: ordinary_data,
    0x0E: photo_info,
    0x0F: photo_content
}


def process_request(proto_no, info_content, client):
    p = int.from_bytes(proto_no, 'big')
    func = proto_dict[p]
    return func(info_content, client)
