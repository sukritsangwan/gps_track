host = "0.0.0.0"
port = 5017

mongo_config = {
    'host': "localhost",
    "port": 27017,
    "db_name": "trackingdb",
    "username": "localUser",
    "password": "protip007",
    "authdb": "admin"
}

logging_config = dict({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '[%(asctime)s] [%(levelname)s] [%(name)s:%(lineno)s] %(message)s'
        },
    },
    'handlers': {
        'default': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': "/tmp/telkonika.log",
            'mode': "a+",
            'formatter': 'standard'
        },
    },
    'root': {
        'handlers': ['default'],
        'level': 'DEBUG',
        'propagate': True
    }
})
