from datetime import datetime
import sys, time
from threading import Thread
import eventlet
import traceback
import logging
from logging.config import dictConfig
import proto_0x6767
import proto_0x7878
import proto_telkonika
from pymongo import MongoClient

import proto_test
import utils

connected_devices = {}
superadmin_ips = ["127.0.0.1", "localhost"]


def handle(client, remote_addr):
    client.imei = "unknown"
    client.protocol_handler = None
    while True:
        try:
            if not client.protocol_handler:
                client.protocol_handler = find_protocol_handler(client, remote_addr)
            else:
                client.protocol_handler.read_header(client, remote_addr)

            # return response
            response = client.protocol_handler.handle(client, remote_addr)

            if not response:
                continue

            client.sendall(response)

            logger.debug({'msg': "Transmit packet", 'imei': client.imei, 'ip': remote_addr[0], 'packet': response})

            connected_devices[client.imei] = client

        except utils.DisconnectError as e:
            break
        except Exception as e:
            traceback.print_exc()
            logger.error({'imei': client.imei, 'ip': remote_addr[0], 'exception': e})
    logger.info({'msg': "Device disconnected", 'ip': remote_addr[0], 'imei': client.imei})
    if client.imei in connected_devices:
        del connected_devices[client.imei]
    client.close()


handler_dict = {
    (0x7878).to_bytes(2, 'big'): proto_0x7878,
    (0x6767).to_bytes(2, 'big'): proto_0x6767,
    (0x8989).to_bytes(2, 'big'): proto_test,
    b'\x00\x0f': proto_telkonika
}


def find_protocol_handler(client, remote_addr):
    c = client.recv(2)
    if not c:
        raise utils.DisconnectError("")
    if c in handler_dict:
        return handler_dict[c]

    raise utils.DisconnectError("protocol not supported")


def dump_status():
    while True:
        try:
            item = {'time': datetime.now(), 'count': len(connected_devices), 'devices': []}
            for imei, client in connected_devices.items():
                dev = {
                    'imei': imei,
                    'interval': 0,
                    'lastUpdated': client.last_time if hasattr(client, 'last_time') else None,
                    'ignition': client.ignition if hasattr(client, 'ignition') else False,
                    'lat': client.lat if hasattr(client, 'lat') else None,
                    'lng': client.lng if hasattr(client, 'lng') else None
                }
                if hasattr(client, 'second_last_time'):
                    dev['interval'] = int((client.last_time - client.second_last_time).total_seconds())
                item['devices'].append(dev)
            utils.mongodb['devices_status'].insert(item)
        except Exception as ex:
            traceback.print_exc()
            logger.error("error in dumping status")
        time.sleep(30)


if __name__ == "__main__":
    config_module = sys.argv[1]

    config = __import__(config_module)

    dictConfig(config.logging_config)
    logger = logging.getLogger(__name__)

    cli = MongoClient(config.mongo_config['host'], config.mongo_config['port'])
    if 'username' in config.mongo_config:
        cli.the_database.authenticate(config.mongo_config['username'], config.mongo_config['password'], source=config.mongo_config['authdb'])
    utils.mongodb = cli[config.mongo_config['db_name']]
    logger.info("mongo connected at " + config.mongo_config['host'] + ":" + str(config.mongo_config['port']))

    logger.info("TCP server starting up on %s:%s" % (config.host, config.port))
    server = eventlet.listen((config.host, config.port))

    pool = eventlet.GreenPool(10000)
    thread = Thread(target=dump_status, daemon=True)
    thread.start()
    os_error_handled = False
    while True:
        try:
            new_connection, address = server.accept()
            logger.info({'msg': "New connection", 'ip': str(address)})
            # writer = new_connection.makefile('w')
            # reader = new_connection.makefile('r')
            pool.spawn_n(handle, new_connection, address)
        except OSError as error:
            if str(error.errno) == "24":
                if os_error_handled:
                    continue
                os_error_handled = True
                # take a snapshot of open file descriptors and log it
                proc_dict = utils.get_open_fds()
                traceback.print_exc()
                logger.error('printing open file descriptors')
                logger.error(proc_dict)
                logger.critical('OSError in GPS server' + '\n' + str(proc_dict))
            else:
                traceback.print_exc()
                logger.exception("error in accepting connection")
        except Exception as ex:
            traceback.print_exc()
            logger.exception("error in accepting connection")
        os_error_handled = False
