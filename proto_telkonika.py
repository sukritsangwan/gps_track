from datetime import datetime
import logging
from utils import dump_data, get_int, DisconnectError

logger = logging.getLogger(__name__)


def read_header(client, remote_addr):
    return


def handle(client, remote_addr):
    if client.imei == "unknown":
        imei = client.recv(15).decode('ascii')
        logger.debug(imei)
        client.imei = imei
        return b'\x01'

    c = client.recv(4)  # zeros
    if len(c) < 4:
        raise DisconnectError("4 zeros not found at beginning")
    logger.debug({'zeros': c})
    length = get_int(client.recv(4))
    logger.debug({'length': length})
    all_data = client.recv(length)
    crc = client.recv(4)
    logger.debug({'crc': crc})

    # check crc, if not ok return 0, else process data
    record_count = process_data(all_data, client)

    return record_count.to_bytes(4, 'big')


def process_data(data_bytes, client):
    logger.debug({'all_data': data_bytes})
    codec_id = data_bytes[0]  # should be 0x08
    n = get_int(data_bytes[1:2])
    i = 2
    while n > 0:
        logger.debug({'i': i, 'n': n})
        record = data_bytes[i:]
        record_length = process_record(record, client)
        i += record_length
        n -= 1
    n_again = get_int(data_bytes[-1:])
    return n_again


def get_crc_16_itu(content_bytes):
    # logger.debug({'msg for crc': content_bytes})
    crc_value = 0xffff
    crc16tab = (
        0x0000, 0x1189, 0x2312, 0x329B, 0x4624, 0x57AD, 0x6536, 0x74BF,
        0x8C48, 0x9DC1, 0xAF5A, 0xBED3, 0xCA6C, 0xDBE5, 0xE97E, 0xF8F7,
        0x1081, 0x0108, 0x3393, 0x221A, 0x56A5, 0x472C, 0x75B7, 0x643E,
        0x9CC9, 0x8D40, 0xBFDB, 0xAE52, 0xDAED, 0xCB64, 0xF9FF, 0xE876,
        0x2102, 0x308B, 0x0210, 0x1399, 0x6726, 0x76AF, 0x4434, 0x55BD,
        0xAD4A, 0xBCC3, 0x8E58, 0x9FD1, 0xEB6E, 0xFAE7, 0xC87C, 0xD9F5,
        0x3183, 0x200A, 0x1291, 0x0318, 0x77A7, 0x662E, 0x54B5, 0x453C,
        0xBDCB, 0xAC42, 0x9ED9, 0x8F50, 0xFBEF, 0xEA66, 0xD8FD, 0xC974,
        0x4204, 0x538D, 0x6116, 0x709F, 0x0420, 0x15A9, 0x2732, 0x36BB,
        0xCE4C, 0xDFC5, 0xED5E, 0xFCD7, 0x8868, 0x99E1, 0xAB7A, 0xBAF3,
        0x5285, 0x430C, 0x7197, 0x601E, 0x14A1, 0x0528, 0x37B3, 0x263A,
        0xDECD, 0xCF44, 0xFDDF, 0xEC56, 0x98E9, 0x8960, 0xBBFB, 0xAA72,
        0x6306, 0x728F, 0x4014, 0x519D, 0x2522, 0x34AB, 0x0630, 0x17B9,
        0xEF4E, 0xFEC7, 0xCC5C, 0xDDD5, 0xA96A, 0xB8E3, 0x8A78, 0x9BF1,
        0x7387, 0x620E, 0x5095, 0x411C, 0x35A3, 0x242A, 0x16B1, 0x0738,
        0xFFCF, 0xEE46, 0xDCDD, 0xCD54, 0xB9EB, 0xA862, 0x9AF9, 0x8B70,
        0x8408, 0x9581, 0xA71A, 0xB693, 0xC22C, 0xD3A5, 0xE13E, 0xF0B7,
        0x0840, 0x19C9, 0x2B52, 0x3ADB, 0x4E64, 0x5FED, 0x6D76, 0x7CFF,
        0x9489, 0x8500, 0xB79B, 0xA612, 0xD2AD, 0xC324, 0xF1BF, 0xE036,
        0x18C1, 0x0948, 0x3BD3, 0x2A5A, 0x5EE5, 0x4F6C, 0x7DF7, 0x6C7E,
        0xA50A, 0xB483, 0x8618, 0x9791, 0xE32E, 0xF2A7, 0xC03C, 0xD1B5,
        0x2942, 0x38CB, 0x0A50, 0x1BD9, 0x6F66, 0x7EEF, 0x4C74, 0x5DFD,
        0xB58B, 0xA402, 0x9699, 0x8710, 0xF3AF, 0xE226, 0xD0BD, 0xC134,
        0x39C3, 0x284A, 0x1AD1, 0x0B58, 0x7FE7, 0x6E6E, 0x5CF5, 0x4D7C,
        0xC60C, 0xD785, 0xE51E, 0xF497, 0x8028, 0x91A1, 0xA33A, 0xB2B3,
        0x4A44, 0x5BCD, 0x6956, 0x78DF, 0x0C60, 0x1DE9, 0x2F72, 0x3EFB,
        0xD68D, 0xC704, 0xF59F, 0xE416, 0x90A9, 0x8120, 0xB3BB, 0xA232,
        0x5AC5, 0x4B4C, 0x79D7, 0x685E, 0x1CE1, 0x0D68, 0x3FF3, 0x2E7A,
        0xE70E, 0xF687, 0xC41C, 0xD595, 0xA12A, 0xB0A3, 0x8238, 0x93B1,
        0x6B46, 0x7ACF, 0x4854, 0x59DD, 0x2D62, 0x3CEB, 0x0E70, 0x1FF9,
        0xF78F, 0xE606, 0xD49D, 0xC514, 0xB1AB, 0xA022, 0x92B9, 0x8330,
        0x7BC7, 0x6A4E, 0x58D5, 0x495C, 0x3DE3, 0x2C6A, 0x1EF1, 0x0F78,
    )
    for ch in content_bytes:
        tmp = crc_value ^ ch
        crc_value = (crc_value >> 8) ^ crc16tab[(tmp & 0xff)]
    return (crc_value ^ 0xFFFF).to_bytes(2, 'big')


def process_record(record, client):
    logger.debug({'record': record})
    time_value = datetime.fromtimestamp(get_int(record[0:8]) / 1000)
    priority = record[8]
    gps_data = record[9:24]
    io_bytes = record[24:]

    lng = get_int(gps_data[0:4]) / 10000000
    lat = get_int(gps_data[4:8]) / 10000000
    alt = get_int(gps_data[8:10])
    direction = get_int(gps_data[10:12])
    no_of_sat = get_int(gps_data[12:13])
    speed = get_int(gps_data[13:15])

    io_event_id, io_data_dict, io_length = process_io_data(io_bytes, client)

    protocol_extra = {'priority': priority, 'no_of_sat': no_of_sat, 'io_event': io_event_id, }

    data_dict = {'time': time_value, 'lat': lat, 'lng': lng, 'altitude': alt, 'speed': speed, 'direction': direction,
                 'protocol_extra': protocol_extra, 'status': io_data_dict}

    dump_data(client.imei, data_dict=data_dict)

    if hasattr(client, 'last_time'):
        client.second_last_time = client.last_time
    client.last_time = time_value
    client.lat = lat
    client.lng = lng

    logger.debug({'imei': client.imei, 'data': data_dict})
    logger.debug({'io_length': io_length})
    return 24 + io_length


def process_io_data(io_bytes, client):
    logger.debug({'io_data': io_bytes})

    io_data_dict = {}

    event_id = get_int(io_bytes[0:1])
    n = get_int(io_bytes[1:2])
    n1 = get_int(io_bytes[2:3])
    cur_index = 3
    io_data_dict.update(extract_io_values(io_bytes[cur_index: cur_index + (n1 * 2)], 1))
    cur_index += n1 * 2
    n2 = get_int(io_bytes[cur_index:cur_index+1])
    cur_index += 1
    io_data_dict.update(extract_io_values(io_bytes[cur_index: cur_index + (n2 * 3)], 2))
    cur_index += n2 * 3
    n4 = get_int(io_bytes[cur_index:cur_index+1])
    cur_index += 1
    io_data_dict.update(extract_io_values(io_bytes[cur_index: cur_index + (n4 * 5)], 4))
    cur_index += n4 * 5
    n8 = get_int(io_bytes[cur_index:cur_index+1])
    cur_index += 1
    io_data_dict.update(extract_io_values(io_bytes[cur_index: cur_index + (n8 * 9)], 8))
    cur_index += n8 * 9

    return event_id, io_data_dict, cur_index


io_id_map = {
    1: "ignition",
    2: "ac",
    3: "din3",
    9: "ain1",
    24: "speed",
    179: "dou1",
    180: "dou2",
    199: "odometer",
    240: "movement",
    241: "op_code"
}


def extract_io_values(data_bytes, val_len):
    io_data_dict = {}
    i = 0
    while i < len(data_bytes):
        io_id = get_int(data_bytes[i:i+1])
        if io_id in io_id_map:
            io_id = io_id_map[io_id]
        else:
            io_id = str(io_id)
        io_val = data_bytes[i + 1: i + 1 + val_len]
        io_data_dict[io_id] = get_int(io_val)
        i += 1 + val_len
    return io_data_dict
