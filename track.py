from datetime import datetime
from utils import dump_data


def process_tracking_data(request):
    req_data = str(request.data)

    for data in req_data.split(';'):
        data_dict = decode_data(data)
        dump_data({'device_id': request.device_id, 'raw_data': data, 'time': data_dict['time'], 'info': data_dict})

    flag = 0x01
    return flag.to_bytes(1, 'big')


def decode_data(data):
    components = data.split('|')

    gprmc = components[0].split(',')
    hdop = components[1]
    alt = components[2]
    cell = components[3].split(',')
    state = components[4]
    alarm = components[5]
    battery = components[6]
    signal = components[7]

    time_str = gprmc[0]
    status = gprmc[1]
    lat = gprmc[2]
    n_s = gprmc[3]
    lng = gprmc[4]
    e_w = gprmc[5]
    knots = gprmc[6]
    heading = gprmc[7]
    date_str = gprmc[8]

    cid = cell[0]
    lac = cell[1]
    mnc = cell[2]
    mcc = cell[3]

    latitude = float(lat[:2]) + (float(lat[2:]) / 60)
    if n_s == 'S':
        latitude *= -1
    longitude = float(lng[:2]) + (float(lng[2:]) / 60)
    if e_w == 'W':
        longitude *= -1

    speed = float(knots) * 1.852

    time = datetime.strptime(date_str+time_str, '%d%m%y%H%M%S.%f')

    return {
        'latitude': latitude, 'longitude': longitude, 'altitude': float(alt),
        'speed': speed, 'heading': heading, 'time': time
    }
