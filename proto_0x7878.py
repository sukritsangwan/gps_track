from datetime import datetime
import logging
import time
import re
import binascii
from utils import DisconnectError, dump_data, get_int, get_bool, get_commands, command_mark_sent

logger = logging.getLogger(__name__)

HEADER = 0x7878.to_bytes(2, 'big')
h_b = 0x78.to_bytes(1, 'big')


def read_header(client, remote_addr):
    while True:
        c = client.recv(1)
        if not c:
            raise DisconnectError("")
        if c == h_b:
            c = client.recv(1)
            if not c:
                raise DisconnectError("")
            if c == h_b:
                return


def handle(client, remote_addr):
    # read length
    len_byte = client.recv(1)
    length = int.from_bytes(len_byte, 'big')

    data_length = length - 5

    proto_no = client.recv(1)

    info_content = client.recv(data_length)

    info_serial_no = client.recv(2)

    req_crc = client.recv(2)
    logger.debug({'header': HEADER, 'length': length, 'proto_no': proto_no, 'info_content': info_content,
                  'info_serial_no': info_serial_no, 'req_crc': req_crc})

    # check crc
    msg = len_byte + proto_no + info_content + info_serial_no
    calc_crc = get_crc_16_itu(msg)
    # logger.debug( binascii.hexlify(calc_crc))
    if calc_crc != req_crc:
        logger.warning("crc not matched. msg: %s, crc: %s, calculated: %s" % (msg, req_crc, get_crc_16_itu(msg)))
        find_eol(client)
        return

    # read stop signal
    find_eol(client)

    # save the info serial no
    client.info_serial_no = info_serial_no

    # process msg
    response_content = process_request(proto_no, info_content, client)

    if response_content is None:
        # no need to send response
        return

    return construct_response(proto_no, response_content, info_serial_no)


def construct_response(protocol, content, serial):
    if content is None:
        # no need to send response
        return

    # calculate response length
    resp_length = len(content) + 5
    len_byte = resp_length.to_bytes(1, 'big')

    # CRC
    msg = len_byte + protocol + content + serial
    crc = get_crc_16_itu(msg)

    # return response
    return HEADER + msg + crc + b'\r\n'


def find_eol(client):
    while True:
        c = client.recv(1)
        if c == b'\r':
            c = client.recv(1)
            if c == b'\n':
                # logger.debug('EOL detected')
                return


def get_crc_16_itu(content_bytes):
    # logger.debug({'msg for crc': content_bytes})
    crc_value = 0xffff
    crc16tab = (
        0x0000, 0x1189, 0x2312, 0x329B, 0x4624, 0x57AD, 0x6536, 0x74BF,
        0x8C48, 0x9DC1, 0xAF5A, 0xBED3, 0xCA6C, 0xDBE5, 0xE97E, 0xF8F7,
        0x1081, 0x0108, 0x3393, 0x221A, 0x56A5, 0x472C, 0x75B7, 0x643E,
        0x9CC9, 0x8D40, 0xBFDB, 0xAE52, 0xDAED, 0xCB64, 0xF9FF, 0xE876,
        0x2102, 0x308B, 0x0210, 0x1399, 0x6726, 0x76AF, 0x4434, 0x55BD,
        0xAD4A, 0xBCC3, 0x8E58, 0x9FD1, 0xEB6E, 0xFAE7, 0xC87C, 0xD9F5,
        0x3183, 0x200A, 0x1291, 0x0318, 0x77A7, 0x662E, 0x54B5, 0x453C,
        0xBDCB, 0xAC42, 0x9ED9, 0x8F50, 0xFBEF, 0xEA66, 0xD8FD, 0xC974,
        0x4204, 0x538D, 0x6116, 0x709F, 0x0420, 0x15A9, 0x2732, 0x36BB,
        0xCE4C, 0xDFC5, 0xED5E, 0xFCD7, 0x8868, 0x99E1, 0xAB7A, 0xBAF3,
        0x5285, 0x430C, 0x7197, 0x601E, 0x14A1, 0x0528, 0x37B3, 0x263A,
        0xDECD, 0xCF44, 0xFDDF, 0xEC56, 0x98E9, 0x8960, 0xBBFB, 0xAA72,
        0x6306, 0x728F, 0x4014, 0x519D, 0x2522, 0x34AB, 0x0630, 0x17B9,
        0xEF4E, 0xFEC7, 0xCC5C, 0xDDD5, 0xA96A, 0xB8E3, 0x8A78, 0x9BF1,
        0x7387, 0x620E, 0x5095, 0x411C, 0x35A3, 0x242A, 0x16B1, 0x0738,
        0xFFCF, 0xEE46, 0xDCDD, 0xCD54, 0xB9EB, 0xA862, 0x9AF9, 0x8B70,
        0x8408, 0x9581, 0xA71A, 0xB693, 0xC22C, 0xD3A5, 0xE13E, 0xF0B7,
        0x0840, 0x19C9, 0x2B52, 0x3ADB, 0x4E64, 0x5FED, 0x6D76, 0x7CFF,
        0x9489, 0x8500, 0xB79B, 0xA612, 0xD2AD, 0xC324, 0xF1BF, 0xE036,
        0x18C1, 0x0948, 0x3BD3, 0x2A5A, 0x5EE5, 0x4F6C, 0x7DF7, 0x6C7E,
        0xA50A, 0xB483, 0x8618, 0x9791, 0xE32E, 0xF2A7, 0xC03C, 0xD1B5,
        0x2942, 0x38CB, 0x0A50, 0x1BD9, 0x6F66, 0x7EEF, 0x4C74, 0x5DFD,
        0xB58B, 0xA402, 0x9699, 0x8710, 0xF3AF, 0xE226, 0xD0BD, 0xC134,
        0x39C3, 0x284A, 0x1AD1, 0x0B58, 0x7FE7, 0x6E6E, 0x5CF5, 0x4D7C,
        0xC60C, 0xD785, 0xE51E, 0xF497, 0x8028, 0x91A1, 0xA33A, 0xB2B3,
        0x4A44, 0x5BCD, 0x6956, 0x78DF, 0x0C60, 0x1DE9, 0x2F72, 0x3EFB,
        0xD68D, 0xC704, 0xF59F, 0xE416, 0x90A9, 0x8120, 0xB3BB, 0xA232,
        0x5AC5, 0x4B4C, 0x79D7, 0x685E, 0x1CE1, 0x0D68, 0x3FF3, 0x2E7A,
        0xE70E, 0xF687, 0xC41C, 0xD595, 0xA12A, 0xB0A3, 0x8238, 0x93B1,
        0x6B46, 0x7ACF, 0x4854, 0x59DD, 0x2D62, 0x3CEB, 0x0E70, 0x1FF9,
        0xF78F, 0xE606, 0xD49D, 0xC514, 0xB1AB, 0xA022, 0x92B9, 0x8330,
        0x7BC7, 0x6A4E, 0x58D5, 0x495C, 0x3DE3, 0x2C6A, 0x1EF1, 0x0F78,
    )
    for ch in content_bytes:
        tmp = crc_value ^ ch
        crc_value = (crc_value >> 8) ^ crc16tab[(tmp & 0xff)]
    return (crc_value ^ 0xFFFF).to_bytes(2, 'big')


def login(data, client):
    if not hasattr(client, 'imei') or client.imei == "unknown":
        imei = binascii.hexlify(data).decode('ascii')[1:]
        client.imei = imei
    logger.info({'imei': client.imei, 'msg': 'Login packet'})
    return b''


def gps_info(data, client):
    # first 6 bit is date time
    # next bit has length of gps data and no of satellite, e.g. 0x9C means length is 9 and no of sat is 12
    time_value = decode_date_time(data[0:6])

    length = data[6] >> 4
    no_of_sat = data[6] & 0b00001111

    lat = float(get_int(data[7:11])) / 1800000
    lng = float(get_int(data[11:15])) / 1800000

    speed = get_int(data[15])
    other_info = bin(get_int(data[16:18]))[2:]
    while len(other_info) < 16:
        other_info = '0' + other_info
    direction = int(other_info[-10:], 2)
    gps_type = 'realtime' if other_info[2] == '0' else 'different'
    is_gps_located = True if other_info[3] == '1' else False
    south_or_north = "N" if other_info[4] == '1' else "S"
    east_or_west = "W" if other_info[5] == '1' else "E"

    if south_or_north == "S":
        lat -= 1
    if east_or_west == "W":
        lng -= 1

    protocol_extra = {'protocol_no': 0x10, 'gps_type': gps_type, 'is_gps_located': is_gps_located,
                      'satellites': no_of_sat}
    status = client.status if hasattr(client, 'status') else None

    data_dict = {
        'time': time_value, 'lat': lat, 'lng': lng, 'speed': speed, 'direction': direction,
        'protocol_extra': protocol_extra, 'gsm_data': None, 'status': status
    }

    dump_data(client.imei, data_dict=data_dict)

    logger.debug({'imei': client.imei, 'data': data_dict})

    check_pending_command(client)


def check_pending_command(client):
    commands = get_commands(client.imei)
    for c in commands:
        send_command(c, client)
        command_mark_sent(c['_id'])


def lbs_info(data, client):
    time_value = decode_date_time(data[0:6])
    mcc = get_int(data[6:8])
    mnc = get_int(data[8])
    lac = get_int(data[9:11])
    cellid = get_int(data[11:14])

    data_dict = {'protocol_no': 0x11, 'time': time_value, 'mcc': mcc, 'mnc': mnc, 'lac': lac, 'cellid': cellid}

    dump_data(client.imei, data_dict=data_dict)

    logger.debug({'imei': client.imei, 'data': data_dict})

    # if len(data) == 14:
    #     return

    # extended format
    # not implemented as of now


def gps_lbs_info(data, client):
    time_value = decode_date_time(data[0:6])

    length = get_int(data[6]) >> 4
    no_of_sat = get_int(data[6]) & 0b00001111

    n = 6 + length

    gps_data = data[6: n]

    lat = float(get_int(gps_data[1:5])) / 1800000
    lng = float(get_int(gps_data[5:9])) / 1800000

    speed = get_int(gps_data[9])
    other_info = bin(get_int(gps_data[10:12]))[2:]
    while len(other_info) < 16:
        other_info = '0' + other_info
    direction = int(other_info[-10:], 2)
    gps_type = 'realtime' if other_info[2] == '1' else 'different'
    is_gps_located = True if other_info[3] == '1' else False
    east_or_west = "W" if other_info[4] == '1' else "E"
    south_or_north = "N" if other_info[5] == '1' else "S"

    if south_or_north == "S":
        lat -= 1
    if east_or_west == "W":
        lng -= 1

    # lbs_data = data[n:]
    mcc = get_int(data[n:n + 2])
    mnc = get_int(data[n + 2])
    lac = get_int(data[n + 3: n + 5])
    cellid = get_int(data[n + 5: n + 8])

    gsm_data = {'mcc': mcc, 'mnc': mnc, 'lac': lac, 'cellid': cellid}
    protocol_extra = {'protocol_no': 0x12, 'gps_type': gps_type, 'is_gps_located': is_gps_located,
                      'satellites': no_of_sat}
    status = client.status if hasattr(client, 'status') else None

    data_dict = {
        'time': time_value, 'lat': lat, 'lng': lng, 'speed': speed, 'direction': direction,
        'protocol_extra': protocol_extra, 'gsm_data': gsm_data, 'status': status
    }

    dump_data(client.imei, data_dict=data_dict)

    if hasattr(client, 'last_time'):
        client.second_last_time = client.last_time
    client.last_time = time_value
    client.lat = lat
    client.lng = lng

    logger.debug({'imei': client.imei, 'data': data_dict})

    check_pending_command(client)


def status_info(data, client):
    device_info = get_int(data[0])
    bin_str = bin(device_info)[2:]
    while len(bin_str) < 8:
        bin_str = '0' + bin_str

    electricity_on = get_bool(bin_str[0])
    gps_located = get_bool(bin_str[1])
    fortified = get_bool(bin_str[-1])
    high_acc = get_bool(bin_str[-2])
    charged = get_bool(bin_str[-3])

    alarm = "normal"
    bin_str = bin_str[2:5]
    if bin_str == "000":
        alarm = "normal"
    elif bin_str == "001":
        alarm = "vibration"
    elif bin_str == "010":
        alarm = "cut-off"
    elif bin_str == "011":
        alarm = "low-power"
    elif bin_str == "100":
        alarm = "sos"

    voltage = get_int(data[1])
    gsm_signal = get_int(data[2])

    data_dict = {'protocol_no': 0x13, 'electricity_on': electricity_on, 'gps_located': gps_located, 'alarm': alarm,
                 'fortified': fortified, 'high_acc': high_acc, 'charged': charged, 'voltage': voltage,
                 'gsm_signal': gsm_signal, 'time': datetime.now()}

    client.status = data_dict

    dump_data(client.imei, data_dict=data_dict)
    client.ignition = high_acc
    client.latest_status = data_dict

    logger.debug({'imei': client.imei, 'data': data_dict})
    return b''


def sat_snr_info(data, client):
    return b''


def strings_info(data, client):
    # this is response of the command from server to device
    # length = int.from_bytes(data[0:1], 'big')
    server_flag = data[1:5]
    msg = data[6:]
    logger.info({'imei': client.imei, 'protocol_no': 0x15, 'msg': str(msg), 'server_flag': server_flag})

    if server_flag == (10).to_bytes(4, 'big'):
        data_dict = parse_current_location(msg)
        if hasattr(client, 'status'):
            data_dict['status'] = client.status
        dump_data(client.imei, data_dict)
        logger.debug({'imei': client.imei, 'data': data_dict})
        if hasattr(client, 'last_time'):
            client.second_last_time = client.last_time
        client.last_time = data_dict['time']
        client.lat = data_dict['lat']
        client.lng = data_dict['lng']
        # if 6 <= datetime.now().hour < 8 or 12 <= datetime.now().hour < 16:
        if not client.stop_cont_track:
            time.sleep(1)
            find_current_location(client)


def parse_current_location(content):
    pattern = ".*Lat:([NS]{1})(\d+\.\d*),Lon:([EW]{1})(\d+\.\d*),Course:(\d+\.\d*),Speed:(\d+\.\d*)" \
              ",DateTime:(\d+-\d+-\d+ \d+:\d+:\d+).*"
    m = re.match(pattern, content.decode("ascii"))
    info = m.groups()
    lat = float(info[1]) * (-1 if info[0] == "S" else 1)
    lng = float(info[3] * (-1 if info[2] == "W" else 1))

    data = {
        "lat": lat,
        "lng": lng,
        "direction": float(info[4]),
        "speed": float(info[5]),
        "time": datetime.strptime(info[6], "%y-%m-%d %H:%M:%S"),

        "protocol_extra": {'protocol_no': 10},
        "gsm_data": None,
        "status": None
    }
    return data


def gps_lbs_status_info(data, client):
    return b''


def cell_location_info(data, client):
    return b''


def lbs_all_info(data, client):
    return b''


def server_to_device(data, client):
    return b''


proto_dict = {
    0x01: login,
    0x10: gps_info,
    0x11: lbs_info,
    0x12: gps_lbs_info,
    0x13: status_info,
    0x14: sat_snr_info,
    0x15: strings_info,
    0x16: gps_lbs_status_info,
    0x17: cell_location_info,
    0x18: lbs_all_info,
    0x80: server_to_device
}


def process_request(proto_no, info_content, client):
    p = int.from_bytes(proto_no, 'big')
    func = proto_dict[p]
    return func(info_content, client)


def find_current_location(client):
    msg = bytes("DWXX,258741#", "ascii")
    server_flag = (10).to_bytes(4, 'big')
    send_packet(msg, client, server_flag)


def send_command(command, client):
    msg = None
    if command['type'] == "raw":
        msg = bytes(command['msg'], 'ascii')
    elif command['type'] == "interval":
        msg = bytes("#at#{}#sum#0#".format(int(command['interval'])), 'ascii')
    elif command['type'] == "START":
        client.stop_cont_track = False
        find_current_location(client)
    elif command['type'] == "STOP":
        client.stop_cont_track = True
    if msg is not None:
        send_packet(msg, client)


def send_packet(msg, client, server_flag=None):
    length = len(msg).to_bytes(1, 'big')
    if server_flag is None:
        server_flag = (1).to_bytes(4, 'big')

    content = length + server_flag + msg

    protocol = 0x80.to_bytes(1, 'big')
    serial = (int.from_bytes(client.info_serial_no, 'big') + 1).to_bytes(2, 'big')
    packet = construct_response(protocol, content, serial)
    client.info_serial_no = serial
    client.sendall(packet)
    logger.info({'msg': "Send command", 'imei': client.imei, 'packet': packet})


def decode_date_time(data):
    # 6 byte each for year, month, day, hour, min and sec
    # For example： 15:50:23 on March 23,2010.
    # The value is 0x0A 0x03 0x17 0x0F 0x32 0x17
    year = 2000 + get_int(data[0])
    month = get_int(data[1])
    day = get_int(data[2])
    hour = get_int(data[3])
    minute = get_int(data[4])
    sec = get_int(data[5])
    if month < 1 or day < 1:
        month = 1
        day = 1
        year = 1971
    return datetime(year, month, day, hour, minute, sec)
