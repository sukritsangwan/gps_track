"""
The data format to be followed when dumping in trackingdb

By default the data is dumped in collection that is named same as device id(imei)
lat and lng are mandatory. If these are not present, data is dumped in another collection,
named by appending "_others" to device id(imei)
"""
from datetime import datetime

data_format = {
    'lat': float,           # latitude -90 to +90, required
    'lng': float,           # longitude -180 to +180, required
    'alt': float,           # altitude(height) in metres, can be int, optional
    'time': datetime,       # the time of gps record
    'speed': int,           # speed of vehicle in km/h, can be float
    'direction': float,     # the direction or heading of moving vehicle, angle from north, 0 to 360

    'gsm_data': {           # data related to gsm signal, optional
        'mcc': int,         # mobile country code
        'mnc': int,         # mobile network code
        'lac': int,         # location area code
        'cellid': int       # the id of cell tower
    },

    'status': {             # optional, but the format should be this only
        'ignition': bool,   # is the vehicle's engine running
        'ac': bool,         # is the AC of vehicle running
        'movement': bool    # movement detected by movement sensor
    },

    'protocol_extra': {}    # can contain any field, only pertaining to a certain protocol
}
