host = "0.0.0.0"
port = 5016

mongo_config = {
    'host': "localhost",
    "port": 27017,
    "db_name": "trackingdb",
    "username": "localUser",
    "password": "protip007",
    "authdb": "admin"
}

logging_config = dict({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '[%(asctime)s] [%(levelname)s] [%(name)s] %(message)s'
        },
    },
    'handlers': {
        'default': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': "/var/log/gps_server/gps_track.log",
            'mode': "a+",
            'formatter': 'standard'
        },
        'mail': {
            'level': 'CRITICAL',
            'class': 'logging.handlers.SMTPHandler',
            'mailhost': ('smtp.gmail.com', 587),
            'fromaddr': 'edufits@gmail.com',
            'toaddrs': ('sukritsangwan@gmail.com',),
            'subject': 'Critical log message from GPS server',
            'credentials': ('edufits@gmail.com', 'nvnygfjvcrwexhkg'),
            'secure': (),
            'formatter': 'standard'
        },
    },
    'root': {
        'handlers': ['default', 'mail'],
        'level': 'INFO',
        'propagate': True
    }
})